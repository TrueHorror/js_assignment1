import {fetchLaptops} from "./api.js"

class App{

    constructor(){
        // Laptop Select variables
        this.elLaptopsSel = document.getElementById("laptopsSel")
        this.elFeaturesList = document.getElementById("featuresList")

        // Laptop Info card 
        this.elLaptopInfoImg = document.getElementById("laptopImg")
        this.elLaptopInfoName = document.getElementById("laptopName")
        this.elLaptopInfoDescription = document.getElementById("laptopDescription")
        this.elLaptopInfoPrice = document.getElementById("laptopPrice")
        this.elLaptopInfoContains = document.getElementById("laptopInfoContainer")

        // Bank variables
        this.elBankCard = document.getElementById("bankCard")
        this.elModal = document.getElementById("modal")
        this.elLoanPrompt = document.getElementById("loanPrompt")
        this.elLoanInput = document.getElementById("loanInput")
        this.elBankCardTextSection = document.getElementById("bankCardTextSection")
        this.elLoanBalanceSection = document.getElementById("loanBalanceSection")
        
        this.haveLoan = false
        
        // Work variables
        this.elWorkCard = document.getElementById("workCard")


        // Balance variables
        this.PAY = 100 // What you get payed for working
        this.bankBalance = 100 // Start with 100 in bank balance
        this.payBalance = 0 // Start with 0 pay balance
        this.loan = 0 // Start with 0 in loan

        //
        this.elBankBalance = document.getElementById("bankBalance")
        this.elWorkBalance = document.getElementById("workBalance")
        this.elLoanBalance = document.getElementById("loanBalance")
        
        // Button variables
        this.elGetALoanBtn = document.getElementById("getALoanBtn")
        this.elTransferToBank = document.getElementById("bankBtn")
        this.elWorkBtn = document.getElementById("workBtn")
        this.elBuyBtn = document.getElementById("buyBtn")
        this.elAcceptLoanBtn = document.getElementById("acceptLoanBtn")
        this.elCloseModalBtn = document.getElementById("closeModalBtn")
        this.elPayBackLoanBtn = document.getElementById("payBackLoanBtn")
        this.elBuyLaptopBtn = document.getElementById("buyLaptopBtn")

        // Laptop variables
        this.laptops = []
        this.selectedLaptop = null
    }
    

    async init(){

        // Fetch laptops from Json-server
        await this.fetchAndAddLaptopsToSelect()

        //Initialize eventListeners
        this.elGetALoanBtn.addEventListener("click", this.handleLoanBtnClick.bind(this))
        this.elAcceptLoanBtn.addEventListener("click", this.handleAcceptLoanBtnClick.bind(this))
        this.elCloseModalBtn.addEventListener("click", this.handleCloseModalBtnClick.bind(this))
        this.elLaptopsSel.addEventListener("change", this.handleLaptopsSelChange.bind(this))
        this.elPayBackLoanBtn.addEventListener("click", this.handlePayBackLoanBtnClick.bind(this))
        this.elWorkBtn.addEventListener("click", this.handleWorkBtnClick.bind(this))
        this.elTransferToBank.addEventListener("click", this.handleTransferToBankBtn.bind(this))
        this.elBuyLaptopBtn.addEventListener("click", this.handleBuyLaptopBtnClick.bind(this))
        

        this.render()

        
    }

    async fetchAndAddLaptopsToSelect(){
        try {
            this.elLaptopsSel.disabled = true
       
            await fetchLaptops()
                .then(laptops => 
                        laptops.map((laptop) => {
                            this.laptops.push(laptop)
                            const elLaptopOption = document.createElement("option")
                            elLaptopOption.value = laptop.id
                            elLaptopOption.innerText = laptop.name
                            this.elLaptopsSel.appendChild(elLaptopOption)
                        })
                        
                )
            
        } catch (e) {
            console.log(e);
        } 
    }

    

    handleLoanBtnClick(){
        if (this.loan > 0) {
            this.removeErrorSection()
            const elPayBackLoanFirstErrorSection = this.createErrorSections(`Sorry, you have a loan, pay back first`)
            this.elBankCard.appendChild(elPayBackLoanFirstErrorSection)
            setTimeout(this.removeErrorSection, 5000)
            
            return
        }
        this.elModal.style.display = "block"
        
    }

    handleAcceptLoanBtnClick(){
        if (this.elLoanInput.value < 1) {
            this.removeErrorSection()
            const elErrorSection = this.createErrorSections(`Loan has to be more than 0!`)
            this.elLoanPrompt.appendChild(elErrorSection)
            return
        }

        if (this.elLoanInput.value > this.bankBalance * 2) {
            this.removeErrorSection()
            const elErrorSection = this.createErrorSections(`Your loan cannot be more than double your balance`)
            this.elLoanPrompt.appendChild(elErrorSection);
            
            return
        }
        
        this.loan = parseInt(this.elLoanInput.value)
        this.haveLoan = true
        this.updateOutstandingLoan()
        this.bankBalance += this.loan;

        this.exitModal();
    }

    handleCloseModalBtnClick(){
        this.exitModal();
    }

    exitModal(){
        this.elModal.style.display = "none"
        this.elLoanInput.value = ""
        this.render()
    }

    handleLaptopsSelChange(){
        this.render()
    }

    setSelectedLaptop(){
        this.selectedLaptop = this.laptops.find(laptop => {
            return laptop.id == this.elLaptopsSel.value
        })
    }

    updateFeatureList(){
        const laptop = this.selectedLaptop
        this.elFeaturesList.innerHTML = ''
        laptop.features.forEach(feature => {
            
            const listItem = document.createElement('li')
            listItem.innerText = feature
            this.elFeaturesList.appendChild(listItem)
        });
        
    }

    createErrorSections(text){
        const elErrorSection = document.createElement('section');
        elErrorSection.id = 'errorSection'
        elErrorSection.innerText = text
        return elErrorSection
    }

    removeErrorSection(){
        if (document.getElementById('errorSection')) {
            document.getElementById('errorSection').remove()
        }
    }

    updateOutstandingLoan(){
        if (this.haveLoan) {
            this.elLoanBalanceSection.style.display = "block"
            return   
        }
        this.elLoanBalanceSection.style.display = "none"

    }

    removeOutstandingLoanSection() {
        if (document.getElementById('outgoingLoan')) {
            document.getElementById('outgoingLoan').remove()
        }
    }

    handlePayBackLoanBtnClick(){
        if (this.haveLoan) {
            if (this.payBalance == 0) {
                this.removeErrorSection()
                const elErrorSection = this.createErrorSections(`Work more! You don't have enough money to pay back the loan.`)
                this.elBankCard.appendChild(elErrorSection)
                setTimeout(this.removeErrorSection, 5000)
            }
            if (this.loan >= this.payBalance) {
                this.loan -= this.payBalance
                this.payBalance -= this.payBalance
                
            }
            if (this.loan < this.payBalance) {
                this.payBalance -= this.loan
                this.loan -= this.loan
                
            }
            if (this.loan == 0) {
                    this.haveLoan = false     
            }  
        }else {
            this.removeErrorSection()
            const elErrorSection = this.createErrorSections(`You don't have a loan to pay back`)
            this.elBankCard.appendChild(elErrorSection)
            setTimeout(this.removeErrorSection, 5000)
        }

        
        this.render()
    }

    handleWorkBtnClick(){
        this.payBalance += this.PAY
        this.render()
    }

    handleTransferToBankBtn(){
        if (this.haveLoan) {
            let deductedPay = this.payBalance*0.1
            this.loan -= deductedPay
            if (this.loan < 0) {
                this.bankBalance += this.loan*-1
                this.haveLoan = false
            }
            this.payBalance -= deductedPay
            this.bankBalance += this.payBalance
            
            
            this.updateOutstandingLoan()
        }else{
            this.bankBalance += this.payBalance
        }
        
        this.payBalance = 0
       
        this.render()
    }

    updateLaptopInfoCard(){
        this.elLaptopInfoImg.src = this.selectedLaptop.img
        this.elLaptopInfoName.innerHTML = this.selectedLaptop.name
        this.elLaptopInfoDescription.innerHTML = this.selectedLaptop.description
        this.elLaptopInfoPrice.innerHTML = this.selectedLaptop.price + 'kr'
    }

    handleBuyLaptopBtnClick(){
        if (this.bankBalance >= this.selectedLaptop.price) {
            this.bankBalance -= this.selectedLaptop.price
            const elErrorSection = this.createErrorSections(`YOU NOW OWN ${this.selectedLaptop.name}.`)
            this.elLaptopInfoContains.appendChild(elErrorSection)
            setTimeout(this.removeErrorSection, 5000)
            this.render()
            return
        }
        const elErrorSection = this.createErrorSections(`Sorry, you don't have enough money in the bank and seems to be poor.`)
        this.elLaptopInfoContains.appendChild(elErrorSection)
        setTimeout(this.removeErrorSection, 5000)
    }

    render(){
        this.setSelectedLaptop()
        this.updateFeatureList()
        this.updateLaptopInfoCard()
        this.updateOutstandingLoan()
        this.elLaptopsSel.disabled = false
        this.elPayBackLoanBtn.style.display = 'none'
        if (this.haveLoan) {
            this.elPayBackLoanBtn.style.display = 'block'
        }

        // Renders balance and outstanding loan
        this.elBankBalance.innerHTML = this.bankBalance + "kr"
        this.elLoanBalance.innerHTML = this.loan + "kr"
        this.elWorkBalance.innerHTML = this.payBalance + "kr"
    }
    
}



new App().init()
