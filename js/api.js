const BASE_URL = 'http://localhost:3000'

function getJsonFromBody(response) {
    return response.json()
}

export function fetchLaptops() {
    return fetch(`${BASE_URL}/laptops`).then(getJsonFromBody)
}